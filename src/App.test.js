import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

// it('should render button group', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   const children = div.children;
//   console.log('kids', children);
//   ReactDOM.unmountComponentAtNode(div);
// });
