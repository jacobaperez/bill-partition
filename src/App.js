import React from 'react';
import './App.css';
import Input from './components/Input';
import ButtonGroup from './components/ButtonGroup';
import InputTotal from './components/InputTotal';
import calculateLengths from './utils/calculateLengths';
import { transform } from '@babel/core';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      inputs: [<Input />],
      values: [],
    };
    // noMoreInputs: false,
    // noMoreRemoving: true,
  }

  handleAddInput = () => {
    const { inputs } = this.state;
    inputs.push(<Input />);
    this.setState({
      inputs,
    });
    // noMoreInputs: inputs.length === 6,
    // noMoreRemoving: false,
  };

  handleRemoveInput = () => {
    const { inputs } = this.state;
    // const noMoreRemoving = inputs.length >= 2;
    // noMoreRemoving,
    inputs.pop();
    this.setState({
      inputs,
    });
  };

  transformInputs = inputs => {
    const newInputs = [];
    for (let i = 0; i < inputs.length; i++) {
      const { value } = inputs[i];
      if (!value) {
        alert('Make all your selections please');
        throw new Error('Inputs are wrong homie');
      }
      newInputs.push(parseInt(value));
    }
    return newInputs;
  };

  handleCalculateLengths = () => {
    const total = parseInt(document.getElementById('total').value);
    const inputs = document.getElementsByClassName('selected');
    try {
      if (!total) throw new Error('Put total in.');
      const newInputs = this.transformInputs(inputs);
      calculateLengths(newInputs, total);
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    const { inputs, noMoreInputs, noMoreRemoving } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <div className="Denomination-inputs">{inputs.map(comp => comp)}</div>
          <ButtonGroup
            handleAddInput={this.handleAddInput}
            handleRemoveInput={this.handleRemoveInput}
            handleCalculateLengths={this.handleCalculateLengths}
          />
          <InputTotal></InputTotal>
        </header>
      </div>
    );
  }
}

export default App;
