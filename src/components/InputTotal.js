import React from 'react';
import './InputTotal.css';

export default function InputTotal() {
  return (
    <div className="input-total">
      <input
        id="total"
        type="number"
        placeholder="Total"
        min="0"
        max="100"
      ></input>
    </div>
  );
}
