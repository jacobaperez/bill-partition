import React from 'react';
import './Input.css';

export default function Input() {
  return (
    <div className="input">
      <select className="selected" name="denomination">
        <option value="" selected disabled>
          Choose
        </option>
        <option value="100">$100</option>
        <option value="50">$50</option>
        <option value="20">$20</option>
        <option value="10">$10</option>
        <option value="5">$5</option>
      </select>
      {/* <input placeholder="bill denomination"></input> */}
    </div>
  );
}
