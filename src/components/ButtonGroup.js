import React from 'react';
import './ButtonGroup.css';

export default function ButtonGroup(props) {
  return (
    <div className="action-buttons">
      {/* disabled={props.noMoreInputs} */}
      <button onClick={props.handleAddInput}>Add Input</button>
      {/* disabled={props.noMoreRemoving} */}
      <button onClick={props.handleRemoveInput}>Remove Input</button>
      <button onClick={props.handleCalculateLengths}>Calculate</button>
    </div>
  );
}
